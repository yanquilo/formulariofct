<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    
</head>

<body>
    <div class="centerme">
        <div class="paper-form">
            <header>
                <h1 class="form-title">A simple form!</h1>
                <p class="note"><b>Please Note:</b> Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi laboriosam dolore vitae eum voluptatum sapiente earum veniam pariatur quisquam, omnis incidunt obcaecati dolorum, doloremque illo ad quae assumenda placeat vero iusto soluta! Obcaecati inventore quidem, voluptatem repellat repellendus ex? Mollitia!</p>
            </header>

            <div class="fill-form">
                <section>
                    <h2 class="section-title">Name and Address</h2>
                    <div class="form-row">
                        <div class="form-input form-input--text">
                            <label for="">First Name</label>
                            <input type="text">
                        </div>
                        <div class="form-input form-input--text">
                            <label for="">Last Name</label>
                            <input type="text">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-input form-input--text">
                            <label for="">Mailing Address</label>
                            <input type="text">
                        </div>
                    </div>
                </section>

                <section>
                    <h2 class="section-title">Additional Information</h2>
                    <h3 class="section-subtitle">Lorem ipsum dolor sit amet.</h3>
                    <div class="form-row">
                        <div class="form-input form-input--text">
                            <label for="">Lorem, ipsum.</label>
                            <input type="text">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-input">
                            <input type="checkbox">
                            <label for="">Lorem, ipsum.</label>
                        </div>
                        <div class="form-input">
                            <input type="checkbox">
                            <label for="">Lorem, ipsum.</label>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-input">
                            <label for="">Please Check One:</label>
                        </div>
                        <div class="form-input">
                            <input type="checkbox">
                            <label for="">Lorem, ipsum.</label>
                        </div>
                        <div class="form-input">
                            <input type="checkbox">
                            <label for="">Lorem, ipsum.</label>
                        </div>
                        <div class="form-input">
                            <input type="checkbox">
                            <label for="">Lorem, ipsum.</label>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-input grow-2">
                            <label for="">Grow 2:</label>
                        </div>
                        <div class="form-input">
                            <input type="checkbox">
                            <label for="">Lorem, ipsum.</label>
                        </div>
                        <div class="form-input">
                            <input type="checkbox">
                            <label for="">Lorem, ipsum.</label>
                        </div>
                    </div>

                </section>

            </div>

            <footer>
                That's it for now...
            </footer>

        </div>
    </div>
</body>

</html>