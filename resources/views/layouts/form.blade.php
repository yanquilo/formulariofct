<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Formulario</title>
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Font-->
    <link rel="stylesheet" type="text/css" href="css/opensans-font.css">
    <link rel="stylesheet" type="text/css" href="fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
    <!-- Main Style Css -->
    <link rel="stylesheet" href="css/style.css" />
</head>

<body>
    @include('partials.navbar')
    <div class="page-content">
        @yield('content')
        <div class="form-v1-content">
            <div class="wizard-form">
                <form class="form-register" method="POST" action="/create/form">
                    @csrf
                    @foreach ($errors->all() as $error)
                    <p class="alert alert-danger">{{$error}}</p>
                    @endforeach

                    @if(Session::has('correcto'))
                    <div class="alert alert-success">
                        {{session::get('correcto')}}
                    </div>
                    @endif
                    <div id="form-total">
                        <!-- SECTION 1 -->
                        @include('partials.section1')

                        <!-- SECTION 2 -->
                        @include('partials.section2')

                        <!-- SECTION 3 -->
                        @include('partials.section3')

                        <!-- SECTION 4 -->
                        @include('partials.section4')
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/jquery.steps.js"></script>
    <script src="js/main.js"></script>


    <script>
        $(document).ready(function() {
            var i = 1;
            $('#add').click(function() {
                i++;
                $('#dynamic_field').append('<tr id="row' + i + '"><td><input type="text" name="nombre" placeholder="Nombre" class="form-control name_list" rows="15" cols="5" /></td><td><input type="text" name="apellido1" placeholder="Apellido1" class="form-control name_list" rows="15" cols="5" /></td></tr><tr id="row' + i + '"><td><input type="text" name="apellido2" placeholder="Apellido2" class="form-control name_list" rows="15" cols="5" /></td><td><textarea name="causa" cols="10" placeholder="Causa"></textarea></td><td><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button></td></tr>');
            });
            $(document).on('click', '.btn_remove', function() {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });
            $('#submit').click(function() {
                $.ajax({
                    url: "name.php",
                    method: "POST",
                    data: $('#add_name').serialize(),
                    success: function(data) {
                        alert(data);
                        $('#add_name')[0].reset();
                    }
                });
            });
        });
    </script>

</body>

</html>