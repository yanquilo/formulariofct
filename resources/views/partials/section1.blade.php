  <h2>
      <p class="step-icon"><span>01</span></p>
      <span class="step-text">Formulario 1</span>
  </h2>
  <section>
      <div class="inner">
          <div class="wizard-header">
              <h3 class="heading">Informe Final</h3>
              <p>MÓDULO PROFESIONAL DE FCT DEL CICLO FORMATIVO DE GRADO </p>
          </div>
          <div class="form-row">
              <div class="form-holder">
                  <fieldset style="width: 100%; margin-left: 50%">
                      <legend style="text-align: center;">Grado</legend>
                      <div class="btn-group btn-group-toggle" data-toggle="buttons">
                          <label class="btn btn-info">
                              <input type="radio" name="grado" value="medio" autocomplete="off"> Medio
                          </label>
                          <label class="btn btn-info active">
                              <input type="radio" name="grado" value="superior" autocomplete="off"> Superior
                          </label>
                      </div>
                  </fieldset>
              </div>

          </div>
          <div class="form-row">
              <div class="form-holder">
                  <fieldset style="width: 165%; margin-left: 20%">
                      <legend style="text-align: center;">Modalidad</legend>
                      <div class="btn-group btn-group-toggle" data-toggle="buttons">
                          <label class="btn btn-info">
                              <input type="radio" name="modalidad" value="precencial" autocomplete="off"> Presencial
                          </label>
                          <label class="btn btn-info active">
                              <input type="radio" name="modalidad" value="semipresencial" autocomplete="off"> Semepresencial
                          </label>
                          <label class="btn btn-info active">
                              <input type="radio" name="modalidad" value="dual" autocomplete="off"> Dual
                          </label>
                      </div>
                  </fieldset>
              </div>
          </div>
          <div class="form-row">
              <div class="form-holder form-holder-2">
                  <fieldset>
                      <legend style="text-align: center;">Nombre del Ciclo</legend>
                      <input type="text" name="ciclo" id="ciclo" class="form-control" required>
                  </fieldset>
              </div>
          </div>
          <div class="form-row">

              <div class="form-holder">

                  <fieldset>
                      <legend style="text-align: center;">Curso Académico</legend>
                      <input type="text" name="curso_academico" id="curso_academico" class="form-control" required>
                  </fieldset>
              </div>
          </div>

          <div class="wizard-header">
              <h3 class="heading">Duración del Periodo de Prácticas</h3>
          </div>
          <div class="form-row form-row-date">
              <div class="form-holder form-holder-2">
                  <div class="form-row">
                      <div class="form-holder">

                          <fieldset>
                              <legend style="text-align: center;">Fecha Inicio</legend>
                              <input type="date" name="fechainicio" id="fechainicio" class="form-control" required>
                          </fieldset>
                      </div>
                      <div class="form-holder">
                          <fieldset>
                              <legend style="text-align: center;">Fecha Fin</legend>
                              <input type="date" name="fechafin" id="fechafin" class="form-control" required>
                          </fieldset>
                      </div>
                  </div>
                  <div style="text-align:center" class="form-group row">
                      <fieldset style="margin-left: 25%;">
                          <legend for="example-number-input" class="col-5 col-form-label">Horas a Realizar</legend>
                          <input style="text-align: center;" class="form-control" type="number" value="42" name="horas" id="horas">
                      </fieldset>

                  </div>
              </div>
          </div>
      </div>
  </section>