  <h2>
      <p class="step-icon"><span>02</span></p>
      <span class="step-text">Formulario 2</span>
  </h2>
  <section>
      <div class="inner">
          <div class="wizard-header">
              <h6 class="heading">Alumnos Matriculados</h6>
            
          </div>
          <div class="form-row">
              <div class="form-holder">
                  <fieldset>
                      <legend>Chicas</legend>
                      <input type="number" name="matriV" id="matriV" class="form-control" required>
                  </fieldset>
              </div>
              <div class="form-holder">
                  <fieldset>
                      <legend>Chicos</legend>
                      <input type="number" name="matriM" id="matriM" class="form-control" required>
                  </fieldset>
              </div>
          </div>
          <div class="wizard-header">
              <h6 class="heading">Alumnos Evaluados Aptos</h6>
          </div>
          <div class="form-row">
              <div class="form-holder">
                  <fieldset>
                      <legend>Chicas</legend>
                      <input type="number" name="aptoV" id="aptoV" class="form-control" required>
                  </fieldset>
              </div>
              <div class="form-holder">
                  <fieldset>
                      <legend>Chicos</legend>
                      <input type="number" name="aptoM" id="aptoM" class="form-control" required>
                  </fieldset>
              </div>
          </div>
          <div class="wizard-header">
              <h6 class="heading">Alumnos Evaluados No Aptos</h6>
          </div>
          <div class="form-row">
              <div class="form-holder">
                  <fieldset>
                      <legend>Chicas</legend>
                      <input type="number" name="noAptoV" id="noAptoV" class="form-control" required>
                  </fieldset>
              </div>
              <div class="form-holder">
                  <fieldset>
                      <legend>Chicos</legend>
                      <input type="number" name="noAptoM" id="noAptoM" class="form-control" required>
                  </fieldset>
              </div>
          </div>
          <div class="wizard-header">
              <h6 class="heading">Alumnos Excentos</h6>
          </div>
          <div class="form-row">
              <div class="form-holder">
                  <fieldset>
                      <legend>Chicas</legend>
                      <input type="number" name="excentoV" id="excentoV" class="form-control" required>
                  </fieldset>
              </div>
              <div class="form-holder">
                  <fieldset>
                      <legend>Chicos</legend>
                      <input type="number" name="excentoM" id="excentoM" class="form-control" required>
                  </fieldset>
              </div>
          </div>
          <div class="wizard-header">
              <h6 class="heading">Alumnos Titulados</h6>
          </div>
          <div class="form-row">
              <div class="form-holder">
                  <fieldset>
                      <legend>Chicas</legend>
                      <input type="number" name="tituladoV" id="tituladoV" class="form-control" required>
                  </fieldset>
              </div>
              <div class="form-holder">
                  <fieldset>
                      <legend>Chicos</legend>
                      <input type="number" name="tituladoM" id="tituladoM" class="form-control" required>
                  </fieldset>
              </div>
          </div>
          <div class="wizard-header">
              <h6 class="heading">Alumnos que se quedan en las empresas</h6>
          </div>
          <div class="form-row">
              <div class="form-holder">
                  <fieldset>
                      <legend>Chicas</legend>
                      <input type="number" name="trabajaV" id="trabajaV" class="form-control" required>
                  </fieldset>
              </div>
              <div class="form-holder">
                  <fieldset>
                      <legend>Chicos</legend>
                      <input type="number" name="trabajaM" id="trabajaM" class="form-control" required>
                  </fieldset>
              </div>
          </div>
          <div class="form-row">
              <div class="form-holder form-holder-2">
                  <fieldset>
                      <legend>Nº de Empresas</legend>
                      <input type="number" name="empresas" id="empresas" class="form-control" required>
                  </fieldset>
              </div>
          </div>
          <div class="form-row">
              <div class="form-holder form-holder-2">
                  <fieldset>
                      <legend>Observaciones</legend>
                      <textarea name="observacion" id="observacion" rows="10" cols="50"></textarea>

                  </fieldset>
              </div>
          </div>
      </div>
  </section>