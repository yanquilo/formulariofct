 <h2>
     <p class="step-icon"><span>03</span></p>
     <span class="step-text">Periodo Extraordinario</span>
 </h2>
 <section>
     <div class="inner">
         <br><br>
         <div class="form-row">
           
                 <div class="table-responsive">
                     <legend>Relación Alumnos que realizaron la FCT en Período Extraordinario</legend> <br>
                     <table class="table table-bordered" id="dynamic_field">
                         <tr>
                             <td><input type="text" name="nombre" placeholder="Nombre" class="form-control name_list" rows="15" cols="5" /></td>
                             <td><input type="text" name="apellido1" placeholder="Apellido1" class="form-control name_list" rows="15" cols="5" /></td>

                         </tr>
                         <tr>
                             <td><input type="text" name="apellido2" placeholder="Apellido2" class="form-control name_list" rows="15" cols="5" /></td>
                             <td><textarea name="causa" cols="10" placeholder="Causa"></textarea></td>
                             <td><button type="button" name="add" id="add" style="font-size:10px" class="btn btn-success"><i class='fas fa-caret-down'></i></button></td>
                         </tr>
                     </table>

                 </div>
             
         </div>


     </div>
 </section>