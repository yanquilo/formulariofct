<h2>
    <p class="step-icon"><span>04</span></p>
    <span class="step-text">Exportar</span>
</h2>
<section>
    <div class="inner"><br>
        <h1>Exportar todos los datos del formulario:</h1>
        <div class="form-row">
            <div class="form-holder">
                <fieldset style="width: 100%; margin-left: 50%">
                    <legend style="text-align: center;">Exportar</legend>
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    
                        <label class="btn btn-info">
                            <a class="btn" href="{{url('/generate-pdf')}}" role="button">PDF</a>
                        </label>
                        <label class="btn btn-info">
                            <a class="btn" href="#" role="button">Excel</a>
                        </label>

                    </div>
                </fieldset>
            </div>

        </div>

        <div class="form-row">
            <div class="form-holder">
                <fieldset style="width: 100%; margin-left: 50%">
                    <legend style="text-align: center;">Guardar</legend>
                    <div style="margin-left: 20%;" class="btn-group btn-group-toggle" data-toggle="buttons">
                        <label class="btn btn-info">
                            <button class="btn btn-submit" type="submit" role="button">Guardar</button>
                        </label>
                    </div>
                </fieldset>
            </div>

        </div>
    </div>
</section>