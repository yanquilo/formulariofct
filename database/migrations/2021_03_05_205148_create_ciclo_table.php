<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCicloTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ciclo', function (Blueprint $table) {
            $table->id();
            $table->enum('grado',['Medio','Superior']);
            $table->enum('modalidad',['Presencial','Semipresencial','Dual']);
            $table->string('ciclo',100);
            $table->integer('curso_academico');
            $table->date('fechaInicio');
            $table->date('fechaFin');
            $table->smallInteger('horasRealizar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ciclo');
    }
}
