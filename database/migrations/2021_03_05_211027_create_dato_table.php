<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ciclo_id');
            $table->tinyInteger('matriculadosV');
            $table->tinyInteger('matriculadosM');
            $table->tinyInteger('aptosV');
            $table->tinyInteger('aptosM');
            $table->tinyInteger('noAptosV');
            $table->tinyInteger('noAptosM');
            $table->tinyInteger('excentosV');
            $table->tinyInteger('excentosM');
            $table->tinyInteger('tituladosV');
            $table->tinyInteger('tituladosM');
            $table->tinyInteger('trabajanV');
            $table->tinyInteger('trabajanM');
            $table->tinyInteger('nºempresas');
            $table->string('observaciones', 250);
            $table->timestamps();
            $table->foreign('ciclo_id')->references('id')->on('ciclo')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dato');
    }
}
