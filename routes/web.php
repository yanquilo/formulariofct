<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\CicloController;
use App\Http\Controllers\PDFController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CicloController::class,'index'])->middleware('auth');

//Verificar que se ha logueado un usuario
Auth::routes(['verify' => 'true']);

Route::group(['middleware' => 'verified'], function () {
    // Rutas a verificar

    //Ruta del home
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    //metodo para guardar el formulario
    Route::post('/create', [App\Http\Controllers\DatosController::class, 'store'])->middleware('auth');

    //ruta para crear el formulario
    Route::get('/create', [App\Http\Controllers\DatosController::class, 'create'])->middleware('auth');

    //Ruta para generar el PDF
    Route::get('/generate-pdf', [PDFController::class, 'generatePDF']);

    //Ruta de roles de usuarios
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('products', ProductController::class);


});


