<?php

namespace App\Http\Controllers;

use App\Models\Dato;
use App\Models\Ciclo;
use App\Models\ExtraPeriodo;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\CicloController;
use Illuminate\Http\Request\DatoFormRequest;
use Symfony\Component\Console\Input\Input;

class DatosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       return view('layouts.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $id_ciclo = app(CicloController::class)->create($request); 
        
        //array con los datos del formulario para guardar en la BBDD
        $datos = DB::table('datos')->insertGetId([
            'ciclo_id'=>$id_ciclo,
            'matriculadosV' => $request['matriV'],
            'matriculadosM' => $request['matriM'],
            'aptosV' => $request['aptoV'],
            'aptosM' => $request['aptoM'],
            'noAptosV' => $request['noAptoV'],
            'noAptosM' => $request['noAptoM'],
            'excentosV' => $request['excentoV'],
            'excentosM' => $request['excentoM'],
            'tituladosV' => $request['tituladoV'],
            'tituladosM' => $request['tituladoM'],
            'trabajanV' => $request['trabajaV'],
            'trabajanM' => $request['trabajaM'],
            'nºempresas' => $request['empresas'],
            'observaciones' => $request['observacion'],
            ]);
       
        $request->session()->flash('correcto', 'Se ha creado el formulario.');
        
        return redirect('/create')->with('status','Formulario guardado: '); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Dato  $dato
     * @return \Illuminate\Http\Response
     */
    public function show(Dato $dato)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Dato  $dato
     * @return \Illuminate\Http\Response
     */
    public function edit(Dato $dato)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Dato  $dato
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dato $dato)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Dato  $dato
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dato $dato)
    {
        //
    }
}
