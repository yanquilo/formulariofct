<?php

namespace App\Http\Controllers;

use App\Models\ExtraPeriodo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\CicloController;

class ExtraPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('layouts.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_ciclo = app(CicloController::class)->create($request);
        //array con los datos del formulario para guardar en la BBDD
        $extra_periodo = DB::table('extra_periodo')->insertGetId([
            'ciclo_id' => $id_ciclo,
            'nombre' => $request['nombre'],
            'apellido1' => $request['apellido1'],
            'apellido2' => $request['apellido2'],
            'causa' => $request['causa'],
        ]);

        $request->session()->flash('correcto', 'Se ha creado el formulario.');

        return redirect('/form/create')->with('status', 'Formulario guardado: '); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ExtraPeriodo  $extraPeriodo
     * @return \Illuminate\Http\Response
     */
    public function show(ExtraPeriodo $extraPeriodo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ExtraPeriodo  $extraPeriodo
     * @return \Illuminate\Http\Response
     */
    public function edit(ExtraPeriodo $extraPeriodo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ExtraPeriodo  $extraPeriodo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExtraPeriodo $extraPeriodo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ExtraPeriodo  $extraPeriodo
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExtraPeriodo $extraPeriodo)
    {
        //
    }
}
