<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExtraPeriodo extends Model
{
    use HasFactory;

    //relación 1:N con la tabla ciclo
    public function ciclo(){
        return $this->belongsTo(Ciclo::class);
    }
}
