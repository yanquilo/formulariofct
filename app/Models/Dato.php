<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dato extends Model
{
    use HasFactory;

    //espesificamos las unicas columnas de la base de datos que se van a guardar
    protected $guarded = [
        'matriculadosV',
        'matriculadosM',
        'aptosV',
        'aptosM',
        'noAptosV',
        'noAptosM',
        'excentosV' => $request['excentoV'],
        'excentosM' => $request['excentoM'],
        'tituladosV' => $request['tituladoV'],
        'tituladosM' => $request['tituladoM'],
        'trabajanV' => $request['trabajaV'],
        'trabajanM' => $request['trabajaM'],
        'nºempresas' => $request['empresas'],
        'observaciones' => $request['observacion'],
    ];

    //Relación 1:1 con la tabla ciclo
    public function ciclo()
    {
        return $this->belongsTo(Ciclo::class);
    }
}
