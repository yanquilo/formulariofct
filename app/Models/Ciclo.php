<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ciclo extends Model
{
    use HasFactory;

    //relación 1:1 con la tabla datos
    public function dato(){
        return $this->hasOne(Dato::class);
    }

    //relación 1:N con la tabla extra_periodo
    public function extra_periodo(){
        return $this->hasMany(ExtraPeriodo::class);
    }
}
